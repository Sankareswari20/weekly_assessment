package com.san.EmployeeRecords.week2;

import java.util.ArrayList;

public class MonthlySalary 
{
public void monthlySalary(ArrayList<Employee> emp) {
		
		System.out.println();
        System.out.println("Employee monthly income along with id: ");
		System.out.print("[");
		for (Employee e : emp) {
			System.out.print(e.getId() + " = " + (float)e.getSalary()/12+ ",");
		}
		System.out.print("]");
	}


}
