package com.san.EmployeeRecords.week2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

public class EmployeeCityCount {
	
public void cityNameCount(ArrayList<Employee> emp) {
		
		ArrayList<String> city = new ArrayList<String>() ;
		
		Set<String> set = new TreeSet<String>();
		
		for (Employee e : emp) {
			city.add(e.getCity());
			set.add(e.getCity());
		}
		System.out.println();
		System.out.println("Employee count from each city: ");
		System.out.print("[");
		for(String c : set) {
			System.out.print(c+" = "+Collections.frequency(city, c)+",");
		}
		System.out.println("]");
	}



}
