package com.san.EmployeeRecords.week2;

import java.util.ArrayList;

public class MainClassEmployee {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Employee> emp = new ArrayList<Employee>();
		//Add Employee Details into the ArrayList

		emp.add(new Employee(1, "Aman", 20, 1100000, "IT", "Delhi"));
		emp.add(new Employee(2, "Bobby", 22, 500000, "HR", "Bombay"));
		emp.add(new Employee(3, "Zoe", 20, 750000, "ADMIN", "Delhi"));
		emp.add(new Employee(4, "Smitha", 21, 1000000, "IT", "Chennai"));
		emp.add(new Employee(5, "Smitha", 24, 1200000, "IT", "Bengaluru"));
		//create Object(reference variable) for 3 Classes
		
		EmployeeNameSorting a1 = new EmployeeNameSorting();
		EmployeeCityCount a2 = new EmployeeCityCount();
		MonthlySalary a3 = new MonthlySalary();

		//Throw illegal Arguments if not Saticified the conditions
		try {

			for (Employee e : emp) {
				if (e.getId() <= 0 || e.getName() == null || e.getAge() <= 0|| e.getSalary() <= 0 || e.getDepartment() == null	|| e.getCity() == null)
				{
					throw new IllegalArgumentException();
				}
			}
          //print employee details
			System.out.println(" Employee Details : ");
			System.out.println(emp+"/t");
		
			
			a1.sortingNames(emp);
			a2.cityNameCount(emp);
			a3.monthlySalary(emp);

		  } catch (IllegalArgumentException e) {
			System.err.println("IllegalArgumentException");
			e.printStackTrace();
		}


	}

}
