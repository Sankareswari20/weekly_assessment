package com.san.book.management;
import java.util.Scanner;


public class BookMain {
	
	public static void main(String[] args) {
		Scanner scanner=new Scanner(System.in);
		MagicOfBooks data=new MagicOfBooks();
		while (true) {
			System.out.println("press 1 To add books\r\n"
					+ "press2.To delete entries from book\r\n" + "press 3.To update a book\r\n"
					+ "press 4.To display all books \r\n" + "press 5.Total count of books\r\n"
					+ "press 6 to search autobiography books" +  "\npress 7.To display by features");

			System.out.println("Please Enter your choice : ");
			int key = scanner.nextInt();

			switch (key) {

			case 1:
				System.out.println("Enter the total no of books you want to add : ");
				int n=scanner.nextInt();

				for(int i=1;i<=n;i++) {
					data.addbook();
				}
				break;

			case 2:
				try {
					data.deletebook();
				}catch(BookCustomerException e) {
					System.out.println(e.getMessage());
				}				
				break;
			case 3:
				try {
					data.updatebook();
					
				}
				catch(BookCustomerException e)
				{
					System.out.println(e.getMessage());
				}
				break;
			case 4:
				try 
				{
					data.displayBookInfo();
				}
				catch(BookCustomerException e)
				{
					
					System.out.println(e.getMessage());
				}
				break;

			case 5:
				try {
					data.count();
					
				}catch(BookCustomerException e){
					System.out.println(e.getMessage());
				}
		
				break;

			case 6:
				try {
					data.autobiography();
					
				}catch(BookCustomerException e) {
					System.out.println(e.getMessage());
					
				}
				
				break;


			case 7:
				System.out.println("Enter your choice : \n 1. Price low to high "
						+ "\n 2.Price high to low \n 3. Best selling");
				int choice = scanner.nextInt();

				switch (choice) {

				case 1 : try {
					data.displayByFeature(1);
				} catch (BookCustomerException e) {
					System.out.println(e.getMessage());
				}
				break;
				case 2:try {
					data.displayByFeature(2);
				} catch (BookCustomerException e) {
					System.out.println(e.getMessage());
				} 
				break;
				case 3:try {
					data.displayByFeature(3);
				} catch (BookCustomerException e) {
					System.out.println(e.getMessage());
				}
				break;
				}

			default:
				System.out.println("You've entered wrong choice.!");

			}

		}

	}


}
